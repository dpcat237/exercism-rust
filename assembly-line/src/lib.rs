// This stub file contains items that aren't used yet; feel free to remove this module attribute
// to enable stricter warnings.
#![allow(unused)]

const HOURLY: u32 = 221;

pub fn production_rate_per_hour(speed: u8) -> f64 {
    if speed == 0 {
        return 0.0;
    }

    let rst = speed as u32 * HOURLY;
    return rst as f64 * percentage(speed)
}

pub fn working_items_per_minute(speed: u8) -> u32 {
    if speed == 0 {
        return 0;
    }

    let rst = speed as u32 * HOURLY;
    return ((rst as f64 * percentage(speed))/60 as f64) as u32
}

pub fn percentage(speed: u8) -> f64 {
    if speed <= 4 {
        return 1.0;
    }
    if speed <= 8 {
        return 0.9;
    }
    return 0.77;
}