#[derive(Debug, PartialEq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

pub fn sublist<T: PartialEq>(_first_list: &[T], _second_list: &[T]) -> Comparison {
    let first_len = _first_list.len();
    let second_len = _second_list.len();

    if first_len == 0 && second_len == 0 {
        return Comparison::Equal
    }
    if first_len == 0 || second_len == 0 {
        if first_len == 0 {
            return Comparison::Sublist
        }
        return Comparison::Superlist
    }

    if first_len == second_len {
        if is_sublist(_first_list,_second_list) {
            return Comparison::Equal
        }
        return Comparison::Unequal
    }

    if first_len > second_len {
        if is_sublist(_first_list,_second_list) {
            return Comparison::Superlist
        }
        return Comparison::Unequal
    }

    if is_sublist(_second_list,_first_list) {
        return Comparison::Sublist
    }
    return Comparison::Unequal
}

// check: https://stackoverflow.com/questions/64226562/check-if-vec-contains-all-elements-from-another-vec
pub fn is_sublist<T: PartialEq>(_first_list: &[T], _second_list: &[T]) -> bool {
    for item in _second_list.iter() {
        if !_first_list.contains(item) {
            return false
        }

    }
    return true
}
